const vscode = require('vscode');
const fs = require('fs');
const { vueTemplate, jsTemplate, cssTemplate } = require("./src/template/index.js");
// const html = require('./snippets/html-snippets.json');

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
	// 添加贡献点
	let disposable = vscode.commands.registerCommand('quick-creation.quickCreation', function (args) {
		// 获取上下文
		console.log('上下文 == ',args);
		useExtension(args)
		.then(()=>{
			vscode.window.showInformationMessage('文件添加成功！');
		})
		.catch((error)=>{
			new Error(error);
		})
	});

	context.subscriptions.push(disposable);
}

function deactivate() {}

const useExtension = (args) => {
	return new Promise((resolve, reject)=>{
		try{
			// 判断是否存在根路径,不存在抛出 打开工作区
			const path = vscode.workspace.rootPath;
			if(!path){
				vscode.window.showErrorMessage('请先打开工作区/文件夹!');
				reject('');
				return;
			}
			const authorInfo = vscode.workspace.getConfiguration('quickCreation');
			// 打开输入框
		  vscode.window.showInputBox({
        ignoreFocusOut: true, // 当焦点移动到编辑器的另一部分或另一个窗口时, 保持输入框打开
        password: false, // 为 true 就表示是密码类型
        prompt: "请输入文件名称", // 文本输入提示
        // value: "hello world", // 默认值, 默认全部选中
        // valueSelection: [6, -1],  // 指定选中的范围
      }).then(value => {
        if (!value || !value?.trim()) {
          vscode.window.showErrorMessage("你输入的是空字符串");
          return;
        };
        // 触发右键菜单的文件路径
				const selectPath = args.path;
				// 文件路径下的index.vue文件
				const fileName = `${value}`;
				const folderPath = `${selectPath.slice(1)}/${value}`;
				const filePath = `${selectPath.slice(1)}/${value}/${value}`;
				// 先判断是否存在这个文件,不存在,使用模板添加,存在,抛出异常
				if(!fs.existsSync(folderPath)){
					// vscode.window.showInformationMessage('正在创建文件!');
					console.log('正在创建文件');
					// 创建文件夹
					fs.mkdirSync(folderPath);
					// 创建对应文件
					fs.writeFileSync(filePath +'.vue', vueTemplate(fileName));
					fs.writeFileSync(filePath +'.js', jsTemplate(fileName,authorInfo));
					fs.writeFileSync(filePath +'.css', cssTemplate());
				}else{
					vscode.window.showErrorMessage('该目录下已存在 该文件夹! ');
					reject('')
					return;
				}
			resolve('');
      });
		}
		catch(error){
			reject(new Error(error))
		}
	})
};

module.exports = {
	activate,
	deactivate
}