# quick-creation README

## 用户安装配置后，点击 首选项 -> 设置 -> 检索 "quickCreation", 配置作者信息

|  参数名  | 参数藐视  |
|  ----  | ----  |
| author  | 作者姓名 |
| copyright  | 著作权所属(公司名字) |
| team  | 所属部门 |
| version  | 版本号 |

![配置信息展示](https://gitee.com/anti-hammer-alliance/quick-creation/raw/master/show.png)

### 生成效果

```javascript
/**
 * 文件概述
 * 详细介绍
 * @public
 * @author xxx
 * @version  v5.0
 * @copyright  xxx有限公司
 * @team  xxx部门
 * @date 2023/11/30 14:17:06
 */
```
