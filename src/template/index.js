/**
 * 将字符串转为驼峰命名
 * @param { String } str 原始字符串
 * @returns 驼峰命名字符串
 */
function toHumpName(str){
  var arr = str.split('-');
  for (var i = 1; i < arr.length; i++) {
    // 循环遍历数组
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].substring(1);
    console.log(arr[i]);
  }
  return arr.join(''); // 字符串给加起来
};

/**
 * vue文件模板
 * @param {*} fileName 
 * @returns 
 */
function vueTemplate(fileName) {
  let humpName = toHumpName(fileName);
  return `<template>
  <div class="${fileName}">
    ${fileName}
  </div>
</template>

<script>
import ${humpName} from "./${fileName}.js";
export default ${humpName}
</script>

<style  scoped>
  @import "./${fileName}.css";
</style>
    `;
}


/**
 * js文件模板
 * @param { String } fileName 文件名称
 * @param { Object } authorInfo 作者信息
 * @returns 
 */
function jsTemplate(fileName,authorInfo) {
  let time = new Date();
  let humpName = toHumpName(fileName);
  return `/**
 * 文件概述
 * 详细介绍
 * @public
 * @author ${authorInfo.author}
 * @version  ${authorInfo.version}
 * @copyright  ${authorInfo.copyright}
 * @team  ${authorInfo.team}
 * @date ${time.toLocaleString()}
 */

export default {
  name: "${humpName}",
  components: {
  },
  data (){
    return {
    };
  },
  created(){
  },
  mounted(){
  },
  methods: {
  },
};`;
}

/**
 * css文件模板
 * @returns 
 */
function cssTemplate() {
  return `@charset "UTF-8";`;
}

module.exports = {
  vueTemplate,
  jsTemplate,
  cssTemplate,
};